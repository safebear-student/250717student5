package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class FramesPage {
    WebDriver driver;
    public FramesPage (WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public Boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Frame Page");
    }
}
