package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/*
* Test Logins using Chrome browser
*/
public class Test01_Login extends BaseTest {
    @Test
    public void testLogin() {
        //Step 1 Confirm we are on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 click on the Login link and the Login page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        //Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }
}